#!/usr/bin/env python3


class Triangle:
    def __init__(self, side, height, name):
        self.side = side
        self.height = height
        self.area = 0.5 * side * height
        self.name = name

    def __eq__(self, other):
        return self.area == other.area

    def __lt__(self, other):
        return self.area < other.area

    def __gt__(self, other):
        return self.area > other.area

    def __le__(self, other):
        return self.area <= other.area

    def __ge__(self, other):
        return self.area >= other.area

    def __ne__(self, other):
        return self.area != other.area


a = Triangle(5, 5, "A")
b = Triangle(1, 2, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))
