#!/usr/bin/env python3


class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __eq__(self, other):
        return self.age == other.age

    def __lt__(self, other):
        return self.age < other.age

    def __gt__(self, other):
        return self.age > other.age

    def __le__(self, other):
        return self.age <= other.age

    def __ge__(self, other):
        return self.age >= other.age

    def __ne__(self, other):
        return self.age != other.age


class Mammal(Animal):
    def __init__(self, name, age, teeth_amount):
        super().__init__(name, age)
        self.teeth_amount = teeth_amount

    def __eq__(self, other):
        return self.teeth_amount == other.teeth_amount

    def __lt__(self, other):
        return self.teeth_amount < other.teeth_amount

    def __gt__(self, other):
        return self.teeth_amount > other.teeth_amount

    def __le__(self, other):
        return self.teeth_amount <= other.teeth_amount

    def __ge__(self, other):
        return self.teeth_amount >= other.teeth_amount

    def __ne__(self, other):
        return self.teeth_amount != other.teeth_amount


class Human(Mammal):
    def __init__(self, name, age, teeth_amount, height):
        super().__init__(name, age, teeth_amount)
        self.height = height

    def __eq__(self, other):
        return self.height == other.height

    def __lt__(self, other):
        return self.height < other.height

    def __gt__(self, other):
        return self.height > other.height

    def __le__(self, other):
        return self.height <= other.height

    def __ge__(self, other):
        return self.height >= other.height

    def __ne__(self, other):
        return self.height != other.height


class Slav(Human):
    def __init__(self, name, age, teeth_amount, height, money):
        super().__init__(name, age, teeth_amount, height)
        self.money = money

    def __eq__(self, other):
        return self.money == other.money

    def __lt__(self, other):
        return self.money < other.money

    def __gt__(self, other):
        return self.money > other.money

    def __le__(self, other):
        return self.money <= other.money

    def __ge__(self, other):
        return self.money >= other.money

    def __ne__(self, other):
        return self.money != other.money


class Bird(Animal):
    def __init__(self, name, age, feather_amount):
        super().__init__(name, age)
        self.feather_amount = feather_amount

    def __eq__(self, other):
        return self.age == other.age

    def __lt__(self, other):
        return self.age < other.age

    def __gt__(self, other):
        return self.age > other.age

    def __le__(self, other):
        return self.age <= other.age

    def __ge__(self, other):
        return self.age >= other.age

    def __ne__(self, other):
        return self.age != other.age


class Stork(Bird):
    def __init__(self, name, age, feather_amount, weight):
        super().__init__(name, age, feather_amount)
        self.weight = weight

    def __eq__(self, other):
        return self.weight == other.weight

    def __lt__(self, other):
        return self.weight < other.weight

    def __gt__(self, other):
        return self.weight > other.weight

    def __le__(self, other):
        return self.weight <= other.weight

    def __ge__(self, other):
        return self.weight >= other.weight

    def __ne__(self, other):
        return self.weight != other.weight


a = Animal("A", 5)
b = Animal("B", 5)

c = Mammal("C", 5, 24)
d = Mammal("D", 5, 26)

e = Human("E", 5, 24, 167)
f = Human("F", 5, 26, 185)

g = Slav("G", 5, 24, 185, 21000)
h = Slav("H", 5, 26, 167, 750)

i = Bird("I", 3, 250)
j = Bird("J", 4, 400)

k = Stork("K", 3, 250, 3)
l = Stork("L", 4, 400, 5)

print("1. A == B = {}".format(a == b))
print("2. C < D = {}".format(c < d))
print("3. E > F = {}".format(e > f))
print("4. G <= H = {}".format(g <= h))
print("5. I >= J = {}".format(i >= j))
print("6. K != L = {}".format(k != l))
