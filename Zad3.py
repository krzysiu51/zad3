#!/usr/bin/env python3


class Cuboid:
    def __init__(self, a_side, b_side, c_side, name):
        self.a_side = a_side
        self.b_side = b_side
        self.c_side = c_side
        self.volume = a_side * b_side * c_side
        self.name = name

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume


a = Cuboid(1, 2, 3, "A")
b = Cuboid(5, 5, 1, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))
